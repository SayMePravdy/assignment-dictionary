%include "words.inc"

%define SIZE 255

extern find_word
extern read_word
extern string_length
extern exit
extern print_string

global _start

section .data

buffer: times SIZE db 0

buffer_error: db "buffer overflow", 0

key_error: db "key_doesn't_exist", 0

section .text

_start:
	mov rdi, buffer
	mov rsi, SIZE
	call read_word

	test rax, rax
	je .buf_err
	mov rdi, rax

	mov rsi, first_word
	push rdx
	call find_word
	pop rdx
	test rax, rax
	je .key_err
	.success:
		mov rdi, rax
		add rdi, 8
		add rdi, 1
		add rdi, rdx
		call print_string
		call exit

	.buf_err:
		mov rdi, buffer_error
		jmp .end

	.key_err:
		mov rdi, key_error
		jmp .end 

	.end:
		call print_err
		call exit


	print_err:
		mov rsi, rdi
		call string_length
		mov rdx, rax
		mov rdi, 2
		mov rax, 1
		syscall
		ret
