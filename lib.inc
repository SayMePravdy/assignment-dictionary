global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy	

section .text
 
; okeeeeeeey, let's go 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov     rdi, rax
    mov     rax, 60        
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax            
	.loop:						; пока не 0 увеличиваем аккумулятор
		
		cmp byte[rdi+rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		ret   


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
	
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 1
	syscall

	ret	


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
	mov rsi, rsp
	mov rax, 1
	mov rdx, 1
	mov rdi, 1
	syscall

	pop rdi
	ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
	jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, 10				; делитель
    mov rsi, rsp            
    dec rsp                 ; чтобы ссылалось на некст байт, а не слово
    mov [rsp], byte 0
	
	.loop:						; когда целая часть не 0
		xor rdx, rdx
		div rdi
		add rdx, '0'
		dec rsp
		mov [rsp], dl
		test rax, rax
		jnz .loop
		
	.end:
		mov rdi, rsp
		push rsi
		call print_string
		pop rsp
		ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi,rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .for_each:
		mov al, [rsi]
		cmp al, [rdi]
		jne .not_equal
		inc rdi
		inc rsi
		test al, al
		jnz .for_each
		mov eax, 1
		ret
	.not_equal:
		xor rax, rax
		ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	mov rax, 0
	mov rdi, 0
	dec rsp
    mov [rsp], al
    mov rsi, rsp
    mov rdx,1
    syscall
    mov al, [rsp]
    inc rsp
    ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	.delete_spaces:			; delete all start spaces symb
		push rdi
		push rsi
		push rdx	
		call read_char
		pop rdx
		pop rsi
		pop rdi	

		cmp rax, 0x09
		je .delete_spaces
		cmp rax, 0x0A
		je .delete_spaces
		cmp rax, 0x20
		je .delete_spaces

	.before_loop:
		xor rdx, rdx			; 0 -> counter
	.read_char_loop:
		cmp rdx, rsi
		je .str_error

		cmp rax, 0x00			; checking the end of word
		je .str_ok
		cmp rax, 0x09
		je .str_ok
		cmp rax, 0x0A
		je .str_ok
		cmp rax, 0x20
		je .str_ok

		mov byte [rdi + rdx], al
		inc rdx

		push rdi
		push rsi
		push rdx
		call read_char
		pop rdx
		pop rsi
		pop rdi	

		jmp .read_char_loop

	.str_ok:
		mov byte [rdi + rdx], 0		; null-term
		mov rax, rdi			; returning adress of buf
		jmp .end
		.str_error:
		xor rax, rax			; 0 -> result
		jmp .end
	.end:	
		ret	

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor	rax,rax
	xor	rdx,rdx
    push rbx
	.loop:
		xor	rbx, 0
		mov	bl, byte[rdi+rdx]
		cmp	bl, '0'
		jb	.end
		cmp	bl, '9'
		ja	.end
		sub	bl, '0'              
		imul rax, 10
		add	rax, rbx
		inc	rdx
		jnz	.loop
	.end:
		pop	rbx
		ret
; в теории можно прописать через string_length, но не имеет особого смысла

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov rdx, 0
	mov rax, 0
	mov rcx, 0
	push rdi
	
	cmp byte[rdi+rcx], '-'
	je .neg
	call parse_uint
	jmp .exit
	
	.neg:
		inc rdi
		call parse_uint
		neg rax
		inc rdx
		
	.exit:
		pop rdi
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r9, 0
    call string_length
    inc rax
    cmp rdx, rax                ; проверка вместимости буффера
    jl .buffer_overflow
	
	.loop:
		mov cl, byte[rdi + r9]
		mov byte[rsi + r9], cl
		inc r9
		cmp r9, rax
		jl .loop
	.end:
		dec rax                     ; уже учитывали 0-теримнатор
		ret
	.buffer_overflow: 
		mov rax, 0
		ret

